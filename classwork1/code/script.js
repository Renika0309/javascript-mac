function createElement(elementName, className = "", content = "", link = "" ){
    const element = document.createElement(elementName);
    if(className !== "" || className !== null ){
       element.className = className;  
    }
    if(content !== "" || content !== null ){
        element.textContent = content;  
     }
     if(link !== "" || link !== null ){
        element.href = link;  
     }
    return element
}
const navItems = ["Головна","продукти", "послуги", "про нас", "контакти"];

const header = createElement("header", "header-top"),
logo = createElement("img", "header-top_logo"),
nav = createElement("ul", "nav"),
lis = navItems.map(function(el){
   const li = createElement("li", "nav-items", el);
   const a = createElement("a", undefined, el, "#");
   li.append(a);
   return li;
}),
container = createElement("div","container"),
containerChild = [createElement("div"),
    createElement("div"),
    createElement("div"),
    createElement("div")],
leftDiv = createElement("div", "left-div"),
rightDiv = createElement("div", "right-div"),
centeRightDiv1 = createElement("div", "center-right-div1"),
centeRightDiv2 = createElement("div", "center-right-div2"),
centeLeftDiv1 = createElement("div", "center-left-div1"),
centeLeftDiv2 = createElement("div", "center-left-div2");

nav.append(...lis)
header.prepend(logo);
header.append(nav);
container.append(...containerChild);
containerChild[0].className = "left-element";
containerChild[0].append(leftDiv);
containerChild[3].className = "right-element";
containerChild[3].append(rightDiv);
containerChild[2].className = "center-right-element";
containerChild[2].append(centeRightDiv1,centeRightDiv2);
containerChild[1].className = "center-right-element";
containerChild[1].append(centeLeftDiv1,centeLeftDiv2);
logo.src = "./img/google.png";

window.onload = () => {
document.body.prepend(container);
document.body.prepend(header);


}
