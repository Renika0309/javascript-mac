window.addEventListener("DOMContentLoaded", () => {
const btn = document.querySelector(".keys"),
    display = document.querySelector(".display > input");

const calc = {
    value1: "",
    value2: "",
    sign: "",
    finsh: false,
    memory:""
}
//функція для показу на дісплей
function show(value, el) {
    el.value = value
}
//Натиск на будь-яку кнопку
btn.addEventListener("click", function (e) {

    if (!e.target.classList.contains("button")) return;
    //Натиск на кнопку с
    if (e.target.classList.contains("c")) {

        calc.value1 = "", calc.value2 = "", calc.sign = "", display.value = ""
    };

    let key = e.target.value;
    //Умова якщо перша кнопка 0
    if (key == "0" && calc.value1 == "") {
        calc.value1 == "";
        return;
    };
    //Умова якщо натиск на кнопки 1.2.3.4.5.6.7.8.9.0.
    if (e.target.classList.contains("black") && !e.target.classList.contains("c")) {
        //Перше число
        if (calc.value2 == "" && calc.sign == "") {
            calc.value1 += key;
            console.log(calc.value1, calc.value2, calc.sign);
            show(calc.value1, display)
         }
         //Друге число
        else if (calc.value1 !== "" && calc.value2 !== "" && calc.finsh) {

            calc.finsh = false;
            calc.value2 = key;
            show(calc.value2, display)

        }
        else {
            calc.value2 += key;
            show(calc.value2, display)
            document.querySelector(".button.orange").disabled = false;
        } console.log(calc.value1, calc.sign, calc.value2);
        return;
    };
    //Умова для натиску кнопок m+ m- mrc
    if (e.target.classList.contains("gray")) {
        if (e.target.value == "m+") {
          calc.memory = display.value;
          display.value = "";
         display.placeholder = "m";
           }
       else if (e.target.value == "m-") {
            calc.memory = calc.memory - display.value;

           
        }
        else {
            
            display.value =calc.memory;
            
            e.target.onclick = calc.memory = 0;
            display.placeholder = "";
            
           return;
        };
    }
    //Умова для натиску кнопок * - + /
    if (e.target.classList.contains("pink")) {
        calc.sign = key;
        console.log(calc.value1, calc.value2, calc.sign);
        return;
    }

    if (key == "=") {
        if (calc.value2 == "") calc.value2 = calc.value1;
        switch (calc.sign) {
            case "+":
                calc.value1 = (+calc.value1) + (+calc.value2);
                break;
            case "-":
                calc.value1 = calc.value1 - calc.value2;
                break;
            case "*":
                calc.value1 = calc.value1 * calc.value2;
                break;
            case "/":
                if (calc.value2 == "0") {

                    calc.value1 = "", calc.value2 = "", calc.sign = "", display.value = "Ошибка";
                    return;
                }
                calc.value1 = calc.value1 / calc.value2;
                break;

        }
        calc.finsh = true;
    }
    show(calc.value1, display)
    console.log(calc.value1, calc.value2, calc.sign);
})
})


