  // Завдання1
  let elem = document.querySelector('#elem');
  let style = getComputedStyle(elem);
  console.log('margin: ' + style.margin); 


   //Завдання2
   //Створюємо змінні
     let counter = 0,
         sec = 0,
         min = 0,
         hrs = 0,
         isClick = false,
         t;
    //шукаємо екран секундоміра
     const screen = document.querySelector(".stopwatch-display");
     const [...p] = screen.getElementsByTagName('span');
     //створення функціі яка збільшує секунду на одиницю
     function tick() {
         sec++;
         if (sec >= 60) {
             sec = 0;
             min++;
             if (min >= 60) {
                 min = 0;
                 hrs++;
             }
         }
     };
    //створення функціі яка додає на монітор числа
     function add() {
         tick();
         p[0].textContent = (hrs > 9 ? hrs : "0" + hrs);
         p[1].textContent = (min > 9 ? min : "0" + min)
         p[2].textContent = (sec > 9 ? sec : "0" + sec);
         timer();
     };
     function timer() {
         t = setTimeout(add, 1000);
     };
     const get = id => document.getElementById(id);
     const bgr = document.querySelector(".container-stopwatch");
      //навішуємо на кнопку старт клік
     get("start").onclick = () => {
        if (isClick === true){
            t = null; 
        }else{
        timer();
        isClick = true};
        
         bgr.classList.remove("black");
         bgr.classList.add("green");
        }
//навішуємо на кнопку стоп клік
 get("stop").onclick = () => {
    isClick = false;
     bgr.classList.remove("green");
     bgr.classList.add("red");
     clearTimeout(t);
     t = null;

 }
 //навішуємо на кнопку ресет клік
 get("reset").onclick = () => {
    t =null;
     sec = 0;
     min = 0;
     hrs = 0;

     bgr.classList.remove("red")
     bgr.classList.remove("green")
     bgr.classList.add("silver")

     p[2].innerText = '00';
     p[1].innerText = '00';
     p[0].innerText = '00';
 };

//Завдання 3
 const div = document.createElement("div");
 document.body.append(div);
 const tel = document.createElement("input");
 tel.placeholder = "000-000-00-00";
 tel.type = tel;
 document.body.append(tel);
 const btn = document.createElement("button");
 btn.innerHTML = 'Save';
 document.body.append(btn);
 const telPattern = /\d{3}-\d{3}-\d{2}-\d{2}/;

 btn.onclick = () => {
 if (telPattern.test(tel.value)) {
     btn.style.background = "green";
     document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg" ;
 }
 else {
    div.textContent = "Phone number is incorrect";
    tel.style.borderColor = "red";
 }
 } 
