//Завдання 4
const btnPrew = document.querySelector("#gallery .buttons .prew");
const btnNext = document.querySelector("#gallery .buttons .next");
const img = document.querySelectorAll("#gallery .photos img");
let i = 0;

btnPrew.onclick = function () {
img[i].className = "";
i--;

if (i < 0) {
i = img.length - 1;
}

img[i].className = "showed";
};

btnNext.onclick = function () {
img[i].className = "";
i++;

if (i >= img.length) {
i = 0;
}

img[i].className = "showed";
};

function carousel() {
for (let i = 0; i < img.length; i++) {
img[i].className = "";
}
i++;

if (i > img.length) {
i = 1;
}

img[i - 1].className = "showed";
setInterval(carousel, 3000);
}
//carousel();